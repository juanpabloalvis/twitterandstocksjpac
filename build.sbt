name := """twitterandstocksjpac"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"
resolvers += "Sonatype Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  //"org.reactivemongo" %% "reactivemongo" % "0.12.1",
  "com.adrianhurt" %% "play-bootstrap" % "1.1-P25-B3",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.12.1",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "com.typesafe.play.extras" %% "iteratees-extras" % "1.5.0",
  "com.typesafe.play.extras" %% "iteratees-extras" % "1.5.0",
  specs2 % Test
)

libraryDependencies += "com.ning" % "async-http-client" % "1.9.29"





