package models


import reactivemongo.bson.Macros.Annotations.Key
import org.joda.time.{DateTime, DateTimeZone}
import reactivemongo.bson.{BSONDateTime, BSONHandler}

/** Created by juanpabloalvis on 25/02/17.
  *
  * @constructor create a new person with a name and age.
  * @param id        the tweet id
  * @param created    the symbol of the stock
  * @param symbol    the symbol of the stock
  * @param user      user who make the tweet
  * @param text      the tweet
  * @param followers the number of followers
  * @param likes     the number of likes
  */
case class Tweetsimple(@Key("_id") id: Option[String], created: DateTime, symbol: String, user: String, text: String, followers: Int, likes: Int)

object Tweetsimple {

  DateTimeZone.setDefault(DateTimeZone.UTC)
  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    def read(time: BSONDateTime) = new DateTime(time.value)
    def write(jdtime: DateTime) = BSONDateTime(jdtime.getMillis)
  }
  import reactivemongo.bson.Macros

  implicit val tweetsimpleHandler = Macros.handler[Tweetsimple]

}
