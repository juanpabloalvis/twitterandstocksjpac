package models

import models.InformationTypeEnum.InformationType
import org.joda.time.{DateTime, DateTimeZone}
import reactivemongo.bson.Macros.Annotations.Key
import reactivemongo.bson.{BSONDateTime, BSONDouble, BSONHandler, BSONReader, BSONString, BSONValue, BSONWriter}

/**
  * Created by Juan Pablo Alvis Colmenares on 10/03/17. mailto:juanpabloalvis@gmail.com
  *
  */
object InformationTypeEnum extends Enumeration {

  sealed trait InformationType{ def name: String }
  case object PRICES extends InformationType { val name = "PRICES" }
  case object TWEETS extends InformationType { val name = "TWEETS" }
  case object NEWS extends InformationType { val name = "NEWS" }
  case object FACEBOOK extends InformationType { val name = "FACEBOOK" }
  case object IMAGES extends InformationType { val name = "IMAGES" }

  val documentType = Seq(PRICES, TWEETS, NEWS, FACEBOOK, IMAGES)

  implicit object DocTypesWriter extends BSONWriter[InformationType, BSONString] {
    def write(t: InformationType): BSONString = BSONString(t.name)
  }

  implicit object DocTypesReader extends BSONReader[BSONValue, InformationType] {
    def read(bson: BSONValue): InformationType = bson match {
      case BSONString("PRICES") => PRICES
      case BSONString("TWEETS") => TWEETS
      case BSONString("NEWS") => NEWS
      case BSONString("FACEBOOK") => FACEBOOK
      case BSONString("IMAGES") => IMAGES
    }
  }
}


sealed trait DocumentType {
  val doctype: InformationTypeEnum.InformationType
}

case class SomePrices(
                       override val doctype: InformationType = InformationTypeEnum.PRICES,
                       date: DateTime,
                       value: BigDecimal,
                       open: BigDecimal,
                       close: BigDecimal
                     ) extends DocumentType

case class SomeTweets(
                       override val doctype: InformationType = InformationTypeEnum.TWEETS,
                       id: Option[String], created: DateTime, symbol: String, user: String, text: String, followers: Int, likes: Int
                     ) extends DocumentType

case class SomeNews(
                     override val doctype: InformationType = InformationTypeEnum.NEWS,
                     url: String,
                     text: String
                   ) extends DocumentType

object SomePrices {

  DateTimeZone.setDefault(DateTimeZone.UTC)

  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    def read(time: BSONDateTime) = new DateTime(time.value)

    def write(jdtime: DateTime) = BSONDateTime(jdtime.getMillis)
  }

  implicit object BigDecimalHandler extends BSONHandler[BSONDouble, BigDecimal] {
    def read(double: BSONDouble) = BigDecimal(double.value)

    def write(bd: BigDecimal) = BSONDouble(bd.toDouble)
  }

  import reactivemongo.bson.Macros

  implicit val somePricesHandler = Macros.handler[SomePrices]


}

object SomeTweets {

  DateTimeZone.setDefault(DateTimeZone.UTC)

  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    def read(time: BSONDateTime) = new DateTime(time.value)

    def write(jdtime: DateTime) = BSONDateTime(jdtime.getMillis)
  }

  import reactivemongo.bson.Macros

  implicit val somTweetsHandler = Macros.handler[SomeTweets]

}

object SomeNews {

  DateTimeZone.setDefault(DateTimeZone.UTC)

  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    def read(time: BSONDateTime) = new DateTime(time.value)

    def write(jdtime: DateTime) = BSONDateTime(jdtime.getMillis)
  }

  import reactivemongo.bson.Macros

  implicit val somNewsHandler = Macros.handler[SomeNews]

}

case class Stock(@Key("_id") _id: Option[String], stockName: String, dateTime: DateTime, infotype: InformationType, documentType: DocumentType)


object Stock {

  import org.joda.time.DateTime

  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    def read(time: BSONDateTime) = new DateTime(time.value)

    def write(jodatime: DateTime) = BSONDateTime(jodatime.getMillis)
  }

  import reactivemongo.bson.Macros

  implicit val documentTypeHandler = Macros.handler[DocumentType]

  import reactivemongo.bson.Macros

  implicit val StockObjectTypeHandler = Macros.handler[Stock]


}


