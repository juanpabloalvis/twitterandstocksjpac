package utils


import java.util.Locale

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}

/**
  * Created by juanpabloalvis on 10/03/17.
  */
object TwitterDateParser {

  private val format: DateTimeFormatter = DateTimeFormat
    .forPattern("EEE MMM dd HH:mm:ss Z yyyy")
    .withLocale(Locale.ENGLISH)

  def parseTwitterUTC(date: String): DateTime ={
    format.parseDateTime(date)
  }

}
