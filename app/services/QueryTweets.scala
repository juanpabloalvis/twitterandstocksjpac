package services

import javax.inject.{Inject, _}

import akka.actor.ActorSystem
import com.google.inject.ImplementedBy
import models.Tweetsimple
import play.api.Logger
import play.api.libs.json.{JsValue, Json}
import play.api.libs.oauth.{ConsumerKey, OAuthCalculator, RequestToken}
import play.api.libs.ws.WSClient
import reactivemongo.api.Cursor.NoSuchResultException
import reactivemongo.api.MongoConnection
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument
import utils.TwitterDateParser

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future, Promise}
import scala.util.{Failure, Random, Success}

/**
  * Created by jpalvis on 8/03/17.
  */

@ImplementedBy(classOf[TwitterFeed])
trait QueryTweets {

  def getTweetsById(id: String): Future[Option[Int]]

  /**
    * Create request to Twitter to get some information by some word
    */
  def queryByWord(query: String, maxTweets: Int): Future[Option[JsValue]]

  /**
    * Create request to Twitter to get some information by user
    */
  def queryByUser(user: String): Future[Option[JsValue]]
}


@Singleton
class TwitterFeed @Inject()
(configuration: play.api.Configuration, ws: WSClient, actorSystem: ActorSystem) extends QueryTweets {

  private val driver = new reactivemongo.api.MongoDriver
  private val connection = driver.connection(List(configuration.getString("mongodb.host").get))
  private val mongoUri = configuration.getString("mongodb.uri").get
  private val db = configuration.getString("mongodb.db").get
  private val host = configuration.getString("mongodb.host").get
  private val collectionName = configuration.getString("mongodb.collection").get

  private val tweetsSimple = Seq(
    Tweetsimple(Some("1"), TwitterDateParser.parseTwitterUTC("Thu Mar 09 23:09:20 +0000 2017"), "stockA", "uno este es un tweet", "@Juan", 168, 99),
    Tweetsimple(Some("2"), TwitterDateParser.parseTwitterUTC("Thu Mar 09 23:09:20 +0000 2017"), "stockB", "dos este es otro tweet", "@JuanDos", 268, 299)
  )

  override def getTweetsById(id: String): Future[Option[Int]] =
    Future(
      tweetsSimple.find(_.text == id).map {
        aValue =>
          if (aValue.text != "") {
            1111
          } else {
            9999
          }
      })


  def queryByWordBKWorking(query: String, maxTweets: Int): Future[Option[JsValue]] = {

    // THE REAL SOLUTION
    // https://github.com/lunatech-labs/play-monad-transformers/blob/master/app/services/UserService.scala
    // https://github.com/lunatech-labs/play-monad-transformers/blob/master/app/controllers/Application1.scala
    // TODO Implement the logic that exists in R in order to get the tweets
    // TODO Split the query to Twitter to re-utilize in other queries, could be complex
    // TODO Create another method that brings the last tweet id.
    @volatile var theMaxId = ""
    val firstTweet = findLastTweetIdByStock(collection, query)
    val theId = firstTweet.onComplete({
      case Success(listInt) => {
        theMaxId = listInt.id.get
      }
      case Failure(exception) => {
        theMaxId = ""
      }
    })

   // 1 to maxTweets foreach { _ =>

    credentials.map {
      //val maxTweetId =
      case (consumerKey, requestToken) =>
        ws.url("https://api.twitter.com/1.1/search/tweets.json")
          .sign(OAuthCalculator(consumerKey, requestToken))
          .withQueryString("q" -> query) //https://dev.twitter.com/rest/reference/get/search/tweets#q
          .withQueryString("result_type" -> "mixed") //https://dev.twitter.com/rest/reference/get/search/tweets#result_type
          .withQueryString("count" -> "100") // https://dev.twitter.com/rest/reference/get/search/tweets#count
          .withQueryString("max_id" -> findLastTweetIdByStockSimple(collection, query)) // Para obtener los tweets previos al tweet max_id:""
          //.withQueryString("since_id" -> "100") // Para obtener los tweets a partir del tweet since_id:""
          .get().map {

          response =>
            if (response.status == 200) {
              //Logger.info("Successful response ")
              val json = response.json
              import play.api.libs.json.Json
              //Logger.info("The response "+json )

              val tweeteslist = json \ "statuses"
              val xs = tweeteslist.as[List[JsValue]]


              xs.map(
                aTweet => {
                  val id = (aTweet \ "id_str").as[String]
                  val created_at = (aTweet \ "created_at").as[String]
                  val text = (aTweet \ "text").as[String]
                  val user = (aTweet \ "user" \ "name").as[String]
                  val followers = (aTweet \ "user" \ "followers_count").as[Int]
                  val favourites = (aTweet \ "user" \ "favourites_count").as[Int]
                  val theTweet = new Tweetsimple(Some(id), TwitterDateParser.parseTwitterUTC(created_at), query, user, text, followers, favourites)
                  collection.insert(theTweet)

                  /*
                  NOTE: This is a sophisticate way to save different type of fields, It Should be use in the future.
                    val someTweets = new SomeTweets(InformationTypeEnum.TWEETS, Some(id), TwitterDateParser.parseTwitterUTC(created_at), query, user, text, followers, favourites)
                    val theStock = new Stock(None, query, TwitterDateParser.parseTwitterUTC(created_at), InformationTypeEnum.TWEETS, someTweets)
                    collection.insert(theStock)
                  */
                })

              val sizeOfTweets = xs.length
              Json.prettyPrint(Json.parse(response.body))
              val aJsonResponse: JsValue = Json.parse(
                s"""
              {
              "state" :"sucesfull",
              "msg" :"tweets saved correctly in the db",
              "found":$sizeOfTweets
              }
              """)
              Some(aJsonResponse)
            } else {
              //throw new Exception(s"Could not retrieve tweets for $query query term")
              Some(Json.parse(
                s"""
              {
              "state" :"error",
              "msg" :"Could not retrieve tweets for $query query term"",
              "found":-1
              }
              """))
            }
        }
    }.getOrElse {
      Future.failed(new Exception("You did not correctly configure the Twitter credentials"))
    }



  }

  def nTimes[T](n: Int, f: T=>T) = {
    val l = List.fill(n)(f)
    l.foldRight(identity: T=>T){ (x, y) => y.andThen(x) }
  }

    def getFirstFuture[T](n: Int, f: T=>T) = {
      val l = List.fill(n)(f)
      l.foldRight(identity: T=>T){ (x, y) => y.andThen(x) }
    }
  override def queryByWord(query: String, maxTweets: Int): Future[Option[JsValue]] = {

    // THE REAL SOLUTION
    // https://github.com/lunatech-labs/play-monad-transformers/blob/master/app/services/UserService.scala
    // https://github.com/lunatech-labs/play-monad-transformers/blob/master/app/controllers/Application1.scala
    // TODO Implement the logic that exists in R in order to get the tweets
    // TODO Split the query to Twitter to re-utilize in other queries, could be complex

    val timesToIterate =if((maxTweets/100)==0) 1 else maxTweets/100
    Logger.info("The times that request arrive")
    //def tmp2(theLastId:String):Future[Option[JsValue]]  =  for{

    def tmp2(theLastId:String): Future[Option[JsValue]] =  {for{
      res<- credentials.map {
        //val maxTweetId =
        case (consumerKey, requestToken) =>
          //val theLastId = findLastTweetIdByStockSimple(collection, query)
          ws.url("https://api.twitter.com/1.1/search/tweets.json")
            .sign(OAuthCalculator(consumerKey, requestToken))
            .withQueryString("q" -> query) //https://dev.twitter.com/rest/reference/get/search/tweets#q
            .withQueryString("result_type" -> "mixed") //https://dev.twitter.com/rest/reference/get/search/tweets#result_type
            .withQueryString("count" -> "100") // https://dev.twitter.com/rest/reference/get/search/tweets#count
            .withQueryString("max_id" -> theLastId) // Para obtener los tweets previos al tweet max_id:""
            //.withQueryString("since_id" -> "100") // Para obtener los tweets a partir del tweet since_id:""
            .get().map {

            response =>
              if (response.status == 200) {

                //Logger.info("Successful response ")
                val json = response.json
                import play.api.libs.json.Json
                //Logger.info("The response "+json )

                val tweeteslist = json \ "statuses"
                val xs = tweeteslist.as[List[JsValue]]


                xs.map(
                  aTweet => {
                    val id = (aTweet \ "id_str").as[String]
                    val created_at = (aTweet \ "created_at").as[String]
                    val text = (aTweet \ "text").as[String]
                    val user = (aTweet \ "user" \ "name").as[String]
                    val followers = (aTweet \ "user" \ "followers_count").as[Int]
                    val favourites = (aTweet \ "user" \ "favourites_count").as[Int]
                    val theTweet = new Tweetsimple(Some(id), TwitterDateParser.parseTwitterUTC(created_at), query, user, text, followers, favourites)
                    collection.insert(theTweet)

                    /*
                    NOTE: This is a sophisticate way to save different type of fields, It Should be use in the future.
                      val someTweets = new SomeTweets(InformationTypeEnum.TWEETS, Some(id), TwitterDateParser.parseTwitterUTC(created_at), query, user, text, followers, favourites)
                      val theStock = new Stock(None, query, TwitterDateParser.parseTwitterUTC(created_at), InformationTypeEnum.TWEETS, someTweets)
                      collection.insert(theStock)
                    */
                  })

                val sizeOfTweets = xs.length
                Json.prettyPrint(Json.parse(response.body))
                val aJsonResponse: JsValue = Json.parse(
                  s"""
              {
              "state" :"sucesfull",
              "msg" :"tweets saved correctly in the db",
              "found":$sizeOfTweets
              }
              """)
                Some(aJsonResponse)
              } else {
                //throw new Exception(s"Could not retrieve tweets for $query query term")
                Some(Json.parse(
                  s"""
              {
              "state" :"error",
              "msg" :"Could not retrieve tweets for $query query term"",
              "found":-1
              }
              """))
              }
          }
      }.getOrElse {
        Future.failed(new Exception("You did not correctly configure the Twitter credentials"))
      }
    }yield res
    }

    var i = 0
    var theIndex = ""
    do {

      i = i + 1
      Logger.info("The times to iterate is " + i + " of " + timesToIterate + ". The last Id is: "+theIndex )
      tmp2(theIndex)
      theIndex = if(i==0)""else findLastTweetIdByStockSimple(collection, query)
      //FIXME Sorry, the concurrent execution is too fast, and dont reach to get the min before the next request
      Thread.sleep(10000)
    }
    while( i< timesToIterate )

    val algo: JsValue = Json.parse(
      s"""
              {
              "state" :"sucesfull",
              "msg" :"tweets saved correctly in the db",
              "found":"$query"
              }
              """)


    val finalme =Future(Some(algo))
    finalme
  }

  def findLastTweetIdByStock(collection: BSONCollection, query: String): Future[Tweetsimple] = {


    //val aQuery = BSONDocument("stockName" -> query,"infotype" -> InformationTypeEnum.TWEETS.name)
    //collection.find(aQuery).sort(BSONDocument("documentType" -> BSONString("id" -> ""))).one[BSONDocument]
    /* Approach 1
      db.getCollection('twitterandstockjpac').findOne({"infotype": "TWEETS", "documentType.id":"840208796261720065"}, {"documentType.id": 1, "_id": 0})
    */
    /* Approach 2
      db.getCollection('twitterandstockjpac').aggregate([
      {$match : {"infotype": "TWEETS", "stockName": "$FB"} },
      {$project: {_id:0, theTweetId: "$documentType.id"} },
      {$group: { _id: null, lastId: { $max: "$theTweetId"} } }
      ])
    */
    //db.twitterandstockjpac.find({symbol:"$FB"}, {_id:1}).sort({_id:-1}).limit(1)
    //collection.find(aQuery).sort( BSONDocument("id" -> 1)).one[BSONDocument]
    //collection.find(BSONDocument("symbol" -> query), BSONDocument("_id" -> 1)).sort(BSONDocument("_id" -> -1)).one[BSONDocument]
    collection.find(BSONDocument("symbol" -> query)).sort(BSONDocument("_id" -> 1)).requireOne[Tweetsimple]
  }




  def findLastTweetIdByStockSimple(collection: BSONCollection, query: String): String = {
    //db.table.find( { symbol : "FB" },{min(id):1} )
    val theLast = collection.find(BSONDocument("symbol" -> query)).sort(BSONDocument("_id" -> 1)).requireOne[Tweetsimple]
    //FIXME Am sorry, I am tired, fixme in the calling to this method,
    var result:String= ""
    try{
      val resultMongo = Await.result(theLast, 8.seconds)
      result = resultMongo .id.get
      Logger.info("Using an await. The id is: "+result)
    }catch{
      case e: Exception =>{
        Logger.error("error: "+e)
        result =""
      }

    }
    result
  }

  // TODO replace the connection with an inner dbFromConnection
  def collection: BSONCollection = Await.result(dbFromConnection(connection, collectionName), 10.seconds)

  def dbFromConnection(connection: MongoConnection, collectionName: String): Future[BSONCollection] =
    connection.database(db).
      map(_.collection(collectionName))

  /**
    * Create request to Twitter to get some Userin
    */
  override def queryByUser(user: String): Future[Option[JsValue]] = {
    credentials.map {
      case (consumerKey, requestToken) =>
        ws.url("https://api.twitter.com/1.1/search/tweets.json")
          .sign(OAuthCalculator(consumerKey, requestToken))
          .withQueryString("screen_name" -> user) //https://dev.twitter.com/rest/reference/get/users/show#screen_name
          .get().map {

          response =>
            if (response.status == 200) {

              val json = response.json

              Some(json)
            } else {
              // TODO Change the onTheFly json by a Real JSON
              Some(Json.parse(
                """
              {
              "state" :"error",
              "msg" :"error gettin the user",
              "founded":-9999
              }
              """))
              // TODO Investigate how to deal with errors in the Future[T]
              throw new Exception(s"Could not retrieve tweets for $user query term")
            }
        }
    }.getOrElse {
      // TODO Investigate how to deal with the Future[T].failed exception
      Future.failed(new Exception("You did not correctly configure the Twitter credentials"))

    }
  }

  private def credentials: Option[(ConsumerKey, RequestToken)] = for {
    apiKey <- configuration.getString("twitter.apiKey")
    apiSecret <- configuration.getString("twitter.apiSecret")
    token <- configuration.getString("twitter.token")
    tokenSecret <- configuration.getString("twitter.tokenSecret")
  } yield (
    ConsumerKey(apiKey, apiSecret),
    RequestToken(token, tokenSecret)
  )

}



