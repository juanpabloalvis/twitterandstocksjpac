package controllers

import javax.inject._

import play.api.Logger
import play.api.libs.ws.WSClient
import play.api.mvc._
import services.QueryTweets
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.JsValue

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.matching.Regex

/**
  * Created by jpalvis on 9/03/17.
  */
class TweetsController @Inject()
(configuration: play.api.Configuration, ws: WSClient, queryTweets: QueryTweets, val messagesApi: MessagesApi)
  extends Controller  with I18nSupport{

  def tweetsCallService(query:String) = Action.async {
    queryTweets.getTweetsById(query).flatMap{
      case None => Future.successful(NotFound("Tweets not found"))
      case Some(response) => {
        if(response==1111){
          Future.successful(Ok("Tweets found with "+1111))
        }else{
          Future.successful(Ok("Tweets found with "+9999))
        }
      }
    }
  }

  /**
    * Create an Action to render information of stocks
    */
  def tweetsBK(query:String, maxTweets: Int) = Action.async {


    queryTweets.queryByWord(query, maxTweets).flatMap{
      case None => Future.successful(NotFound("""{"response":"Tweets not found"}"""))
      case Some(response) => {
        Future.successful(Ok(response))
      }
    }
  }
  /**
    * Create an Action to render information of stocks
    */
  def tweets(query:String, maxTweets: Int) = Action.async {


    queryTweets.queryByWord(query, maxTweets).flatMap{
      case None => Future.successful(NotFound("""{"response":"Tweets not found"}"""))
      case Some(response) => {
        Future.successful(
          //Ok(response)
          Redirect(routes.TweetsController.index())
          )
      }
    }
  }

  /**
    * Create an Action to render information of stocks
    */
  def tweetsbyuser(user:String) = Action.async {
    queryTweets.queryByUser(user).flatMap{
      case None => Future.successful(NotFound("""{"response":"Twitter user not found"}"""))
      case Some(response) => {
        Future.successful(Ok(response))
      }
    }
  }

  def index = Action {
    Ok(views.html.Tweets.index(tweetForm))
  }


  def indexPost(ss:String) = Action {
    Ok("PostOk")
  }

  def tweetPost = Action { implicit request =>
    tweetForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.Tweets.index(formWithErrors))
        //NotFound("<h1>Oh no, ocurrió un fallo</h1>"+formWithErrors)
      },
      data => {
        //println(data.symbol)
        Redirect(routes.TweetsController.tweets(data.symbol, data.maxTweets))
      }
    )
  }

  def redirectRServer = Action {
    implicit request =>
      tweetForm.bindFromRequest().fold(
        formWithErrors => {
          // binding failure, you retrieve the form containing errors:
          //BadRequest(views.html.test(formWithErrors))
          NotFound("<h1>Page not found o formulario con errores</h1>"+formWithErrors)
        },
        tweetData => {
          /* en caso de exito redireccionamos a una ruta en vez de entregar una plantilla,
           * este patron es llamado "Redirect after POST", es una forma exelente de prevenir envio duplicado de POST.
           * */
          // val newUser = models.User(userData.name, userData.age)
          // val id = models.User.create(newUser)
          //Redirect(routes.Application.home(id))
          //Redirect(routes.DividendsController.hello(("ddmm")))

        })
      Redirect("http://127.0.0.1:7785")
    //Ok(views.html.Tweets.index(userForm))
  }

  val pattern: Regex = ("\\$([A-Z])\\w+").r
  // The form
  val tweetForm:Form[TweetsData] = Form(
    mapping(
      //"symbol" -> nonEmptyText.verifying(symbol=>pattern.pattern.matcher(symbol).matches()),
      "symbol" -> nonEmptyText,
      "maxTweets" -> number(min = 100, max = 20000)
    )(TweetsData.apply)(TweetsData.unapply)
  )

}

// The class that represents the Form to make the binding with the controller and view
case class TweetsData(symbol: String, maxTweets: Int)

